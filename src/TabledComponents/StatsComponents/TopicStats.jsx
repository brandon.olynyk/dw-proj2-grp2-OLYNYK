function compare(a, b) {
    if (a.nberPost < b.nberPost){
        return 1;
    }
    if (a.nberPost > b.nberPost){
        return -1;
    }
    return 0;
}

function generateTopicStats(categoriesData) {
    let allTopicsArray = [];

    categoriesData.map((cat) => {
        cat.topicList.map((topic) => {
            allTopicsArray.push(topic)
        })
    })

    let sorted = allTopicsArray.sort(compare);

    return (
        <tbody id="topic-stats-tbody">
            {
                sorted.map((topic) => {
                    return (
                        <tr id={topic.id} key={topic.topic_title}>
                            <td>{topic.topic_title}</td>
                            <td>{topic.nberPost}</td>
                            <td>{topic.status}</td>
                        </tr>
                    )
                })
            }
        </tbody>
    )
}

const TopicStats = (props) => {
    let categoriesData = props.categories;
    return (
        <div id="topic-stats">
            <p>Topic Stats</p>
            <table>
                <thead>
                    <tr>
                        <th>Topic Title</th>
                        <th>nberPost</th>
                        <th>Status</th>
                    </tr>
                </thead>
                {generateTopicStats(categoriesData)}
            </table>
        </div>
    );
}
 
export default TopicStats;