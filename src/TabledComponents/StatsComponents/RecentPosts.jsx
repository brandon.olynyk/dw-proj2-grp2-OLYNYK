function compare(a, b) {
    let c = new Date(a.date.replace(' ', 'T'));
    let d = new Date(b.date.replace(' ', 'T'));

    if (c < d) {
        return 1;
    }
    if (c > d) {
        return -1;
    }
    return 0;
}

function generateRecentPosts(categoriesData) {
    let allPostsArray = [];

    categoriesData.map(cat => {
        cat.topicList.map(topic => {
            topic.listPosts.map(post => {
                allPostsArray.push(post)
            })
        })
    })

    let sorted = allPostsArray.sort(compare);

    return (
        <tbody id="recent-posts-tbody">
            {
                sorted.map(post => { 
                    return (
                        <tr id={post.id} key={post.text}>
                            <td>{post.author}</td>
                            <td>{post.date}</td>
                            <td>{post.rate}</td>
                        </tr>
                    )
                })
            }
        </tbody>
    )
}

const RecentPosts = (props) => {
    let categoriesData = props.categories;
    return (
        <div id="recent-posts">
            <p>Recent Posts</p>
            <table>
                <thead>
                    <tr>
                        <th>Author</th>
                        <th>Date</th>
                        <th>Rate</th>
                    </tr>
                </thead>
                {generateRecentPosts(categoriesData)}
            </table>
        </div>
    );
}
 
export default RecentPosts;