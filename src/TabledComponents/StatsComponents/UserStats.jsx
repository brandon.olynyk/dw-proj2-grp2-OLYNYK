function generateUserStats(userData) {
    const sorted = userData.sort((a, b) => b.nberPosts - a.nberPosts)
    return (
        <tbody id="user-stats-tbody">
            {
                sorted.map(user => {
                    return (
                        <tr id={user.user_id} key={user.user_id}>
                            <td>{user.user_id}</td>
                            <td>{user.nberPosts}</td>
                        </tr>
                    )
                })
            }
        </tbody>
    )
}

const UserStats = (props) => {
    let userData = props.users;
    return (
        <div id="user-stats">
                <p>User Stats</p>
                <table id="user-stats-table">
                    <thead>
                        <tr>
                            <th>User</th>
                            <th>nberPost</th>
                        </tr>
                    </thead>
                    {generateUserStats(userData)}
                </table>
            </div>
    );
}
 
export default UserStats;