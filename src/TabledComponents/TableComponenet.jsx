//we might need a class or something for this one
//the table he provided has the tablecomponent going
//into column start and column end
//maybe something to do with the classes?

import RecentPosts from "./StatsComponents/RecentPosts";
import TopicStats from "./StatsComponents/TopicStats";
import UserStats from "./StatsComponents/UserStats";

const TableComponenet = (props) => {
    return (
        <section id="stat-section">
            <TopicStats categories={props.categories} />
            <RecentPosts categories={props.categories} />
            <UserStats users={props.users} />
        </section>
    );
}

export default TableComponenet;