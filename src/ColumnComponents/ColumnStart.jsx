const ColumnStart = () => {
    return ( 
    //dummy admin commands
    //Also added an id to this section so that I can apply a background color - Karekin
    <section id="admin-container">
        <section id="admin-panel">
            <label>ADMIN</label>
            <button >Create Category</button>
            <button>Create Topic</button>
            <button>Close Topic</button>
            <button>Delete Topic</button>
        </section>

    </section> );
}
 
export default ColumnStart;