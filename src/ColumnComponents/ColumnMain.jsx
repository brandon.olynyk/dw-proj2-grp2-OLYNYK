import TopSettings from "../SelectionCmpnts/TopSettings";


const ColumnMain = (props) => {

    return ( <section id="main-column">

    <TopSettings categories={props.categories} searchBarValue={props.searchBarValue}/>

    </section>);
}
 
export default ColumnMain;