import TableComponenet from "../TabledComponents/TableComponenet";

const ColumnEnd = (props) => {
    return ( 
        //Added id to section for color - Karekin
        <section id="test-content-container">
            <p>Test content End</p>
            <TableComponenet users={props.users} categories={props.categories}/>
        </section> );
}
 
export default ColumnEnd;