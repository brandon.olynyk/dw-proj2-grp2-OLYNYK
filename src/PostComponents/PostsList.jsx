import { useEffect } from "react";
import { useState } from "react";

const PostsList = (props) => {
    const [posts, setPosts] = useState(props.list);

    useEffect(() => {
        //update posts
        let newPosts = [];
        props.categories.map(cat => {
            cat.topicList.map(topic => {
                topic.listPosts.map(post => {
                    if(post.text.toLowerCase().includes(props.searchBarValue) && props.searchBarValue !== ""){
                        console.log(props.searchBarValue)
                        newPosts.push(post);              
                    }
                })
            })
        })
        if(props.searchBarValue === ""){
            setPosts(props.list)               
        }else {
            setPosts(newPosts)
        }
    },[props.searchBarValue]);

    
    useEffect(() => {
        setPosts(props.list);
    }, [props.list]);

    const onPostLikeclick = (postId, num) => {
        setPosts(posts.map(post => {
            if(post.id !== postId)
                 return post
            return {...post, like: post.like + num }
        }));
    }
    
    const onPostDelete = (postId) => {
        setPosts(posts.filter(post => {
            if(post.id !== postId) {
                return true
            }
            return false
            
        }));
    }

    return (
        <section id="post-display-table">{
        posts.map(post => {
            return(
                <section className="post-display" key={post.name}>
                    <section className="post-header">
                        <p>{post.text}</p>
                        <p className="post-likes">Likes: {post.like}</p>
                        <img className="image-icon" src={process.env.PUBLIC_URL+"like.png"} onClick={() => {onPostLikeclick(post.id,1)}} alt="like post"></img>
                        <img className="image-icon" src={process.env.PUBLIC_URL+"dislike.png"} onClick={() => {onPostLikeclick(post.id,-1)}} alt="dislike post"></img>   
                    </section>
                    <section className="post-footer">
                        <p className="post-user">{post.author}</p>
                        <p className="post-date">{post.date}</p>
                        <p>{post.replies}</p>
                        <img className="image-icon" src={process.env.PUBLIC_URL+"trash.png"} onClick={() => {onPostDelete(post.id)}} alt="delete po"></img>
                    </section>
                </section>
            );
        })
        }</section>    
    )
}

export default PostsList;