import React,{useCallback, useEffect, useState } from 'react';
import PostsList from '../PostComponents/PostsList';


const DropDown = (props) => {

    let forum_data = props.categories;

    const [selectedCategory, setSelectedCategory]=useState(forum_data[0]);
    const [selectedTopic, setSelectedTopic]=useState(forum_data[0].topicList[0]);
    const [topicOptions, setTopicOptions]=useState(forum_data[0].topicList);
    

    useEffect(() => {

        if(forum_data && selectedCategory)
        {
            let topicArray = selectedCategory.topicList;
        
            setTopicOptions(topicArray);
        }
    }, [selectedCategory]);

    const onCategoryChange = useCallback((event) => {
        const cat = forum_data.find(category => (category.id === Number (event.target.value)));
        setSelectedCategory(cat);
        setSelectedTopic(cat.topicList[0]);
        });

    const onTopicChange = useCallback((event) => {
        setSelectedTopic(selectedCategory.topicList.find(topic => (topic.id === Number(event.target.value))));
    });

    return (
        <>
            <section id="dropdown-menu">
            {/*Included sections here for css purposes -Karekin*/}
            <section id="select_Box_Category">
                <label className="selectLabels">Category Select: </label>
                <select id="category-select" onChange={onCategoryChange} value={selectedCategory.id}>
                {forum_data.map(category => (<option value={category.id}>{category.name}</option>))}
                </select>
            </section>

            <section id="select_Box_Topic">
                <label className="selectLabels">Topic Select: </label>
                <select id="topic-select" onChange={onTopicChange} value={selectedTopic.id}>
                        {topicOptions.map(topic => (<option value={topic.id}>{topic.topic_title}</option>))}
                </select>
            </section>
            </section>
            <PostsList categories={forum_data} list={selectedTopic.listPosts} searchBarValue={props.searchBarValue}/>
        </>        
    );
}
 
export default DropDown;