import { useState, useCallback } from "react";

const SearchBar = (props) => {
    
    const [searchQuery, setSearchQuery] = useState(props.searchBarValue)
    let setSearchBarValue = props.setSearchBarValue;
    const onSearchChange = useCallback((event) => {
        setSearchQuery(event.target.value.toLowerCase());
        setSearchBarValue(searchQuery);
    },[searchQuery, setSearchBarValue]);

    return ( <input type="text" id="searchbar" onChange={onSearchChange} value={searchQuery} placeholder="Search..."></input> );
}
 
export default SearchBar;