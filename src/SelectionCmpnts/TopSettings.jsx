import DropDown from "../DropDownCmpnt/DropDown";

const TopSettings = (props) => {
    
    
    return ( 
        <DropDown categories={props.categories} generateTable={props.generateTable} searchBarValue={props.searchBarValue}/>
    );
}
 
export default TopSettings;