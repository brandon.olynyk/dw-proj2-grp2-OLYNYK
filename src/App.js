import { useCallback, useEffect, useState } from 'react';
import './App.css';
import Body from './AppComponents/Body';
import Footer from './AppComponents/Footer';
import Header from './AppComponents/Header';


function App() {

  const [searchBarValue, setSearchBarValue] = useState("");

  return (
    <div className="App">
      <Header setSearchBarValue={setSearchBarValue}/>
      <Body searchBarValue={searchBarValue}/>
      <Footer/>
    </div>
  );
}

export default App;
