import ColumnEnd from "../ColumnComponents/ColumnEnd";
import ColumnMain from "../ColumnComponents/ColumnMain";
import ColumnStart from "../ColumnComponents/ColumnStart";

import {useState, useEffect} from 'react';


const Body = (props) => {
    const [users, setUsers] = useState([]);
    const [categories, setCategories] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [isLoadingUsers, setIsLoadingUsers] = useState(true);
    


    useEffect(()=>{
        fetch("https://sonic.dawsoncollege.qc.ca/~nasro/js320/project2/users-data.php")
          .then((resp) => {
            if (resp.status===200)
                return resp.json()
          })
          .then((data) => {setUsers(data.users)})
          .catch((error) => console.log("Error: " + error))
          .finally(() => setIsLoading(false));
    },[])

    useEffect(()=>{
        fetch("https://sonic.dawsoncollege.qc.ca/~nasro/js320/project2/forum-data.php")
        .then((response) => {
            if (response.status===200)
                return response.json()
        })
        .then((res) => {setCategories(res.categories)})
        .catch((error) => console.log("Error: " + error))
        .finally(() => setIsLoadingUsers(false));
    },[]);

    if(isLoading || isLoadingUsers){
        return( <p>Loading...</p> );
    }else{
    return ( <section id="main-section">
        <ColumnStart/>
        <ColumnMain categories={categories} searchBarValue={props.searchBarValue}/>
        <ColumnEnd users={users} categories={categories}/>
    </section> );
    }
}
 
export default Body;